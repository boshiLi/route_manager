import 'package:flutter/material.dart';
import 'route_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: RouteManager.getRouteKey(RouteType.home),
      routes: kRoutes,
    );
  }
}
