import 'package:flutter/material.dart';
import 'screen0.dart';
import 'screen1.dart';
import 'screen2.dart';

enum RouteType { home, first, second }

class RouteManager {
  static String getRouteKey(RouteType type) {
    switch (type) {
      case RouteType.home:
        return '/';
      case RouteType.first:
        return '/first';
      case RouteType.second:
        return '/second';
      default:
        return null;
    }
  }

  static void push(RouteType route, BuildContext context) {
    Navigator.pushNamed(context, RouteManager.getRouteKey(route));
  }
}

Map<String, WidgetBuilder> kRoutes = {
  RouteManager.getRouteKey(RouteType.home): (context) => Screen0(),
  RouteManager.getRouteKey(RouteType.first): (context) => Screen1(),
  RouteManager.getRouteKey(RouteType.second): (context) => Screen2(),
};
